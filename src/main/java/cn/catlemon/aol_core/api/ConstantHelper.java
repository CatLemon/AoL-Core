package cn.catlemon.aol_core.api;

import java.util.UUID;

import net.minecraftforge.common.util.Constants;

public final class ConstantHelper {
	public static class HPModifier {
		public static final int ADD_BASE_HP = Constants.AttributeModifierOperation.ADD;
		public static final int ADD_PERCENTAGE_HP = Constants.AttributeModifierOperation.ADD_MULTIPLE;
		public static final int ADD_HP_BUFF = Constants.AttributeModifierOperation.MULTIPLY;
		
		public static final UUID VANILLA_BASE_HP_MODIFER_UUID = UUID.fromString("af8cedb7-dfe2-baf4-c549-23a6fcfb9050");
	}
}
