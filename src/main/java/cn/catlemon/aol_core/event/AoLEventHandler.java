package cn.catlemon.aol_core.event;

import cn.catlemon.aol_core.config.AoLCoreConfig;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;

public final class AoLEventHandler {
	
	public AoLEventHandler(FMLInitializationEvent event) {
		AoLEventHandler.loadUnreloadableHandlers();
		AoLEventHandler.reloadHandlers();
	}
	
	private static HPOverhaulHandler aolHPRelatedHandler;
	private static VanillaRegenerationHandler vanillaRegenerationHandler;
	
	public static void loadUnreloadableHandlers() {
		
	}
	
	public static void reloadHandlers() {
		if (AoLCoreConfig.MainConfig.mechanics.regenerationOverhaul) {
			if (aolHPRelatedHandler == null) {
				aolHPRelatedHandler = new HPOverhaulHandler();
			}
			if (vanillaRegenerationHandler != null) {
				vanillaRegenerationHandler.finalize();
			}
		} else {
			if (aolHPRelatedHandler != null) {
				aolHPRelatedHandler.finalize();
			}
			if (vanillaRegenerationHandler == null) {
				vanillaRegenerationHandler = new VanillaRegenerationHandler();
			}
		}
	}
}
