package cn.catlemon.aol_core.event;

import cn.catlemon.aol_core.api.ConstantHelper;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;

public final class VanillaRegenerationHandler {
	public VanillaRegenerationHandler() {
		net.minecraftforge.common.MinecraftForge.EVENT_BUS.register(this);
	}
	
	public void finalize() {
		net.minecraftforge.common.MinecraftForge.EVENT_BUS.unregister(this);
	}
	
	@SubscribeEvent
	public void onPlayerLoggedIn(PlayerLoggedInEvent event) {
		switchRegeneration(event);
		modifyHPLimit(event);
	}
	
	public static void switchRegeneration(PlayerLoggedInEvent event) {
		event.player.getEntityWorld().getGameRules().setOrCreateGameRule("naturalRegeneration", "true");
	}
	
	public static void modifyHPLimit(PlayerLoggedInEvent event) {
		EntityPlayer player = event.player;
		IAttributeInstance attr = player.getAttributeMap().getAttributeInstance(SharedMonsterAttributes.MAX_HEALTH);
		AttributeModifier modifier = new AttributeModifier(ConstantHelper.HPModifier.VANILLA_BASE_HP_MODIFER_UUID,
				"decrease_base_hp", -10, ConstantHelper.HPModifier.ADD_BASE_HP);
		if (attr.hasModifier(modifier))
			attr.removeModifier(modifier);
	}
}
